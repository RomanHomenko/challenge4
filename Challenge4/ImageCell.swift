//
//  ImageCell.swift
//  Challenge4
//
//  Created by Роман Хоменко on 09.04.2022.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet var photoImage: UIImageView!
    @IBOutlet var imageNameLabel: UILabel!
    
}
