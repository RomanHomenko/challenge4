//
//  ViewController.swift
//  Challenge4
//
//  Created by Роман Хоменко on 09.04.2022.
//

import UIKit

class ViewController: UICollectionViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var images: [Image] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingView()
        
        fetchImagesFromUserDefaults()
    }
}

extension ViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Image", for: indexPath) as? ImageCell else {
            fatalError("Unable dequeue ImageCell")
        }
        let image = images[indexPath.item]
        
        cell.imageNameLabel.text = image.photoDescription
        
        let path = getDocumentsDirectory().appendingPathComponent(image.photoImageName)
        cell.photoImage.image = UIImage(contentsOfFile: path.path)
        
        cell.photoImage.layer.borderColor = UIColor(white: 0, alpha: 0.3).cgColor
        cell.photoImage.layer.borderWidth = 2
        cell.photoImage.layer.cornerRadius = 3
        cell.layer.borderColor = UIColor(white: 0, alpha: 0.3).cgColor
        cell.layer.borderWidth = 2
        cell.layer.cornerRadius = 7
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let image = images[indexPath.item]
        let path = getDocumentsDirectory().appendingPathComponent(image.photoImageName)
        
        let alert = UIAlertController(title: "Give a name for this picture", message: nil, preferredStyle: .alert)
        alert.addTextField()
        alert.addAction(UIAlertAction(title: "Rename", style: .default, handler: { [weak self, weak alert] _ in
            guard let name = alert?.textFields?[0].text else { return }
            image.photoDescription = name
            self?.save()
            collectionView.reloadData()
        }))
        alert.addAction(UIAlertAction(title: "Detail", style: .cancel, handler: { [weak self] _ in
            if let vc = self?.storyboard?.instantiateViewController(withIdentifier: "Detail") as? DetailViewController {
                
                vc.contentOfFile = path.path
                vc.photoDescription = image.photoDescription
                
                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }))
        present(alert, animated: true)
    }
}

extension ViewController {
    func settingView() {
        title = "Photo namer"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(takePhoto))
    }
}

extension ViewController {
    @objc func takePhoto() {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        picker.sourceType = .camera
        present(picker, animated: true)
    }
}

extension ViewController {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let imageFromPicker = info[.editedImage] as? UIImage else { return }
        
        let imageName = UUID().uuidString
        let imagePath = getDocumentsDirectory().appendingPathComponent(imageName)
        
        if let jpegData = imageFromPicker.jpegData(compressionQuality: 0.8) {
            try? jpegData.write(to: imagePath)
        }
        
        let image = Image(imageName: imageName, photoDescription: "Unknown")
        images.append(image)
        save()
        collectionView.reloadData()
        
        dismiss(animated: true)
    }
    
    func getDocumentsDirectory() -> URL {
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        return path[0]
    }
}

// MARK: - Save into and Fetch from UserDefaults

extension ViewController {
    func save() {
        let jsonEncoder = JSONEncoder()
        
        if let savedData = try? jsonEncoder.encode(images) {
            let defaults = UserDefaults.standard
            defaults.set(savedData, forKey: "images")
        } else {
            print("Faild to save images")
        }
    }
    
    func fetchImagesFromUserDefaults() {
        let defaults = UserDefaults.standard
        if let savedImages = defaults.object(forKey: "images") as? Data {
            let jsonDecoder = JSONDecoder()
            
            do {
                images = try jsonDecoder.decode([Image].self, from: savedImages)
            } catch let error {
                print(error)
            }
        }
    }
}
