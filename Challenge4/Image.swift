//
//  Image.swift
//  Challenge4
//
//  Created by Роман Хоменко on 09.04.2022.
//

import Foundation

class Image: NSObject, Codable {
    var photoImageName: String
    var photoDescription: String
    
    init(imageName: String, photoDescription: String) {
        self.photoImageName = imageName
        self.photoDescription = photoDescription
    }
}
