//
//  DetailViewController.swift
//  Challenge4
//
//  Created by Роман Хоменко on 09.04.2022.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var photoDescriptionLabel: UILabel!
    
    var contentOfFile: String?
    
    var selectedImageName: String?
    var photoDescription: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.largeTitleDisplayMode = .never
        
        if let imageToLoad = UIImage(contentsOfFile: contentOfFile!) {
            imageView.image = imageToLoad
        }
        photoDescriptionLabel.text = photoDescription
    }
}
